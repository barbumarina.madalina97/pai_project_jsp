package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.lang.*;
import java.math.*;
import db.*;
import java.sql.*;
import java.io.*;
import java.util.*;

public final class nou_005fDepartament_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html; charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <title>Adauga departament</title>\n");
      out.write("    </head>\n");
      out.write("    ");
      db.JavaBean jb = null;
      synchronized (session) {
        jb = (db.JavaBean) _jspx_page_context.getAttribute("jb", PageContext.SESSION_SCOPE);
        if (jb == null){
          jb = new db.JavaBean();
          _jspx_page_context.setAttribute("jb", jb, PageContext.SESSION_SCOPE);
        }
      }
      out.write("\n");
      out.write("    ");
      org.apache.jasper.runtime.JspRuntimeLibrary.introspect(_jspx_page_context.findAttribute("jb"), request);
      out.write("\n");
      out.write("    <body>\n");
      out.write("        ");

            int idprofesor, idcatedra;
            String id1, id2, NumeProfesor, PrenumeProfesor, Mail, NumeCatedra, SefCatedra, NumeDepartament, SefDepartament;
            id1 = request.getParameter("idprofesor");
            id2 = request.getParameter("idcatedra");
            NumeDepartament = request.getParameter("NumeDepartament");
            SefDepartament = request.getParameter("SefDepartament");
            if (id1 != null) {
                jb.connect();
                jb.adaugaDepartament(java.lang.Integer.parseInt(id1), java.lang.Integer.parseInt(id2), NumeDepartament, SefDepartament);
                jb.disconnect();
        
      out.write("\n");
      out.write("        <p>Datele au fost adaugate.</p>");

        } else {
        jb.connect();
        ResultSet rs1 = jb.vedeTabela("profesori");
        ResultSet rs2 = jb.vedeTabela("catedre");
        
      out.write("\n");
      out.write("        <h1> Suntem in tabela departamente.</h1>\n");
      out.write("        <form action=\"nou_Departament.jsp\" method=\"post\">\n");
      out.write("            <table>\n");
      out.write("                <tr>\n");
      out.write("                    <td align=\"right\">IdProfesor:</td>\n");
      out.write("                    <td> \n");
      out.write("                        Selectati profesor:\n");
      out.write("\t\t\t<SELECT NAME=\"idprofesor\">\n");
      out.write("                                ");

                                    while(rs1.next()){
                                        idprofesor = rs1.getInt("idprofesor");
                                        NumeProfesor = rs1.getString("Nume");
                                        PrenumeProfesor = rs1.getString("Prenume");
                                        Mail = rs1.getString("Mail");
                                
      out.write("\n");
      out.write("                                    <OPTION VALUE=\"");
      out.print( idprofesor);
      out.write('"');
      out.write('>');
      out.print( idprofesor);
      out.write(',');
      out.print( NumeProfesor);
      out.write(',');
      out.print( PrenumeProfesor);
      out.write(',');
      out.print( Mail);
      out.write("</OPTION>\n");
      out.write("                                ");

                                    }
                                
      out.write("\n");
      out.write("\t\t\t</SELECT>\n");
      out.write("                    \n");
      out.write("                    </td>\n");
      out.write("                </tr>\n");
      out.write("                <tr>\n");
      out.write("                    <td align=\"right\">IdCatedra:</td>\n");
      out.write("                    <td> \n");
      out.write("                        Selectati catedra:\n");
      out.write("\t\t\t<SELECT NAME=\"idcatedra\">\n");
      out.write("\t\t\t\t<!-- OPTION selected=\"yes\" VALUE=\"iris1\">Iris 1</OPTION -->\n");
      out.write("                                ");

                                    while(rs2.next()){
                                        idcatedra = rs2.getInt("idcatedra");
                                        NumeCatedra = rs2.getString("Nume");
                                        SefCatedra = rs2.getString("SefCatedra");
                                
      out.write("\n");
      out.write("                                    <OPTION VALUE=\"");
      out.print( idcatedra);
      out.write('"');
      out.write('>');
      out.print( idcatedra);
      out.write(',');
      out.print( NumeCatedra);
      out.write(',');
      out.print( SefCatedra);
      out.write("</OPTION>\n");
      out.write("                                ");

                                    }
                                
      out.write("\n");
      out.write("\t\t\t</SELECT>\n");
      out.write("                     </td>\n");
      out.write("                </tr>\n");
      out.write("                <tr>\n");
      out.write("                    <td align=\"right\">Nume Departament:</td>\n");
      out.write("                    <td> <input type=\"text\" name=\"NumeDepartament\" size=\"30\" /></td>\n");
      out.write("                </tr>\n");
      out.write("                <tr>\n");
      out.write("                    <td align=\"right\">Sef Departament:</td>\n");
      out.write("                    <td> <input type=\"text\" name=\"SefDepartament\" size=\"30\" /></td>\n");
      out.write("                </tr>\n");
      out.write("            </table>\n");
      out.write("            <input type=\"submit\" value=\"Adauga departamentul\" />\n");
      out.write("        </form>\n");
      out.write("        ");

            }
        
      out.write("\n");
      out.write("        <br/>\n");
      out.write("        <a href=\"index.html\"><b>Home</b></a>\n");
      out.write("        <br/>\n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
