package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.lang.*;
import java.math.*;
import db.*;
import java.sql.*;
import java.io.*;
import java.util.*;

public final class modifica_005fDepartament_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html; charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <title>Tabela Departamente</title>\n");
      out.write("    </head>\n");
      out.write("    ");
      db.JavaBean jb = null;
      synchronized (session) {
        jb = (db.JavaBean) _jspx_page_context.getAttribute("jb", PageContext.SESSION_SCOPE);
        if (jb == null){
          jb = new db.JavaBean();
          _jspx_page_context.setAttribute("jb", jb, PageContext.SESSION_SCOPE);
        }
      }
      out.write("\n");
      out.write("    ");
      org.apache.jasper.runtime.JspRuntimeLibrary.introspect(_jspx_page_context.findAttribute("jb"), request);
      out.write("\n");
      out.write("    <body>\n");
      out.write("        <h1 align=\"center\"> Tabela Departamente:</h1>\n");
      out.write("        <br/>\n");
      out.write("        <p align=\"center\"><a href=\"nou_Departament.jsp\"><b>Adauga un nou departament.</b></a> <a href=\"index.html\"><b>Home</b></a></p>\n");
      out.write("        <form action=\"m1_Departament.jsp\" method=\"post\">\n");
      out.write("            <table border=\"1\" align=\"center\">\n");
      out.write("                <tr>\n");
      out.write("                    <td><b>Mark:</b></td>\n");
      out.write("                    <td><b>IdDepartament:</b></td>\n");
      out.write("                    <td><b>IdProfesor:</b></td>\n");
      out.write("                    <td><b>NumeProfesor:</b></td>\n");
      out.write("                    <td><b>PrenumeProfesor:</b></td>\n");
      out.write("                    <td><b>Mail:</b></td>\n");
      out.write("                    <td><b>IdCatedra:</b></td>\n");
      out.write("                    <td><b>NumeCatedra:</b></td>\n");
      out.write("                    <td><b>SefCatedra:</b></td>\n");
      out.write("                    <td><b>NumeDepartament:</b></td>\n");
      out.write("                    <td><b>SefDepartament:</b></td>\n");
      out.write("                </tr>\n");
      out.write("                ");

                    jb.connect();
                    ResultSet rs = jb.vedeDepartamente();
                    long x;
                    while (rs.next()) {
                        x = rs.getInt("iddepartament");
                
      out.write("\n");
      out.write("                <tr>\n");
      out.write("                    <td><input type=\"checkbox\" name=\"primarykey\" value=\"");
      out.print( x);
      out.write("\" /></td><td>");
      out.print( x);
      out.write("</td>\n");
      out.write("                    <td>");
      out.print( rs.getInt("idprofesor"));
      out.write("</td>\n");
      out.write("                    <td>");
      out.print( rs.getString("NumeProfesor"));
      out.write("</td>\n");
      out.write("                    <td>");
      out.print( rs.getString("PrenumeProfesor"));
      out.write("</td>\n");
      out.write("                    <td>");
      out.print( rs.getString("Mail"));
      out.write("</td>\n");
      out.write("                    <td>");
      out.print( rs.getInt("idcatedra"));
      out.write("</td>\n");
      out.write("                    <td>");
      out.print( rs.getString("NumeCatedra"));
      out.write("</td>\n");
      out.write("                    <td>");
      out.print( rs.getString("SefCatedra"));
      out.write("</td>\n");
      out.write("                    <td>");
      out.print( rs.getString("numedepartament"));
      out.write("</td>\n");
      out.write("                    <td>");
      out.print( rs.getString("sefdepartament"));
      out.write("</td>\n");
      out.write("                    ");

                        }
                    
      out.write("\n");
      out.write("                </tr>\n");
      out.write("            </table><br/>\n");
      out.write("            <p align=\"center\">\n");
      out.write("                <input type=\"submit\" value=\"Modifica linia\">\n");
      out.write("            </p>\n");
      out.write("        </form>\n");
      out.write("        ");

    jb.disconnect();
      out.write("\n");
      out.write("        <br/>\n");
      out.write("        <p align=\"center\">\n");
      out.write("            <a href=\"index.html\"><b>Home</b></a>\n");
      out.write("            <br/>\n");
      out.write("        </p>\n");
      out.write("    </body>\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
