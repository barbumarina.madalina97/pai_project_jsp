package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.lang.*;
import java.math.*;
import db.*;
import java.sql.*;
import java.io.*;
import java.util.*;

public final class tabela_005fProfesori_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html; charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n");
      out.write("<head>\n");
      out.write("     <style> body {\n");
      out.write("    background-image: url(\"profesor.png\");\n");
      out.write("    background-repeat: no-repeat;\n");
      out.write("    background-position: center;\n");
      out.write("    background-attachment: fixed;\n");
      out.write("    background-size: cover;\n");
      out.write("}\n");
      out.write("</style>\n");
      out.write("  <meta charset=\"utf-8\">\n");
      out.write("  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n");
      out.write("  <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css\" type=\"text/css\">\n");
      out.write("  <link rel=\"stylesheet\" href=\"https://v40.pingendo.com/assets/bootstrap/bootstrap-4.0.0-beta.1.css\" type=\"text/css\"> </head>\n");
      out.write("\n");
      out.write("\n");
      out.write("    ");
      db.JavaBean jb = null;
      synchronized (session) {
        jb = (db.JavaBean) _jspx_page_context.getAttribute("jb", PageContext.SESSION_SCOPE);
        if (jb == null){
          jb = new db.JavaBean();
          _jspx_page_context.setAttribute("jb", jb, PageContext.SESSION_SCOPE);
        }
      }
      out.write("\n");
      out.write("    ");
      org.apache.jasper.runtime.JspRuntimeLibrary.introspect(_jspx_page_context.findAttribute("jb"), request);
      out.write("\n");
      out.write("    <body>    \n");
      out.write("        <div class=\"py-5\">\n");
      out.write("    <div class=\"container\">\n");
      out.write("      <div class=\"row d-flex justify-content-center\">\n");
      out.write("        <div class=\"col-md-4\">\n");
      out.write("          <h2 class=\"text-center bg-light\">Tabela Profesori</h2>\n");
      out.write("        </div>\n");
      out.write("      </div>\n");
      out.write("    </div>\n");
      out.write("  </div>\n");
      out.write("\n");
      out.write("  <div class=\"text-center text-capitalize\">\n");
      out.write("    <div class=\"container\">\n");
      out.write("      <div class=\"row\">\n");
      out.write("        <div class=\"col-md-12\">\n");
      out.write("          <ul class=\"nav nav-pills align-items-center align-self-center text-center d-flex justify-content-center\">\n");
      out.write("            <li class=\"nav-item text-center align-items-center justify-content-center align-self-center d-flex\">\n");
      out.write("              <a href=\"index.html\" class=\"nav-link active\"> <i class=\"fa fa-home fa-home pull-left\"></i>&nbsp;Pagina principala</a>\n");
      out.write("            </li>\n");
      out.write("          </ul>\n");
      out.write("        </div>\n");
      out.write("      </div>\n");
      out.write("    </div>\n");
      out.write("  </div>\n");
      out.write("             \n");
      out.write("             <div class=\"py-5\">\n");
      out.write("    <div class=\"container\">\n");
      out.write("      <div class=\"row\">\n");
      out.write("        <div class=\"col-md-4 d-flex justify-content-end\">\n");
      out.write("          <a href=\"tabela_Departamente.jsp\" class=\"btn btn-primary\">Tabela Departamente</a>\n");
      out.write("        </div>\n");
      out.write("        <div class=\"col-md-4 d-flex justify-content-center\">\n");
      out.write("          <a href=\"nou_Profesor.jsp\" class=\"btn btn-primary\">Adauga un profesor nou</a>\n");
      out.write("        </div>\n");
      out.write("        <div class=\"col-md-4\">\n");
      out.write("          <a href=\"tabela_Catedre.jsp\" class=\"btn btn-primary\">Tabela Catedre</a>\n");
      out.write("        </div>\n");
      out.write("      </div>\n");
      out.write("    </div>\n");
      out.write("  </div>\n");
      out.write("  \n");
      out.write("        \n");
      out.write("  <form action=\"sterge_Profesor.jsp\" method=\"post\">            \n");
      out.write("   <div class=\"py-5\">\n");
      out.write("    <div class=\"container\">\n");
      out.write("      <div class=\"row\">\n");
      out.write("        <div class=\"col-md-12\">\n");
      out.write("          <table class=\"table opaque-overlay\">\n");
      out.write("              <thead>\n");
      out.write("              <tr>\n");
      out.write("                <th class=\"bg-light\">Mark:</th>\n");
      out.write("                <th class=\"bg-light\">ID Profesort:</th>\n");
      out.write("                <th class=\"bg-light\">Nume:</th>\n");
      out.write("                <th class=\"bg-light\">Prenume:</th>\n");
      out.write("                <th class=\"bg-light\">Mail:</th>\n");
      out.write("                </tr>\n");
      out.write("              </thead>\n");
      out.write("            \n");
      out.write("              \n");
      out.write("            ");

                    jb.connect();
                    ResultSet rs = jb.vedeTabela("profesori");
                    long x;
                    while (rs.next()) {
                        x = rs.getInt("idprofesor");
                
      out.write("\n");
      out.write("                <tbody>\n");
      out.write("              <tr>\n");
      out.write("                    <td class=\"bg-light\"><input type=\"checkbox\" name=\"primarykey\" value=\"");
      out.print( x);
      out.write("\" /></td><td class=\"bg-light\">");
      out.print( x);
      out.write("</td>\n");
      out.write("                    <td class=\"bg-light\">");
      out.print( rs.getString("Nume"));
      out.write("</td>\n");
      out.write("                    <td class=\"bg-light\">");
      out.print( rs.getString("Prenume"));
      out.write("</td>\n");
      out.write("                    <td class=\"bg-light\">");
      out.print( rs.getString("Mail"));
      out.write("</td>\n");
      out.write("                      \n");
      out.write("                      ");

                        }
                    
      out.write("\n");
      out.write("                    </tr>\n");
      out.write("                </tbody>  \n");
      out.write("\n");
      out.write("          </table>\n");
      out.write("        </div>\n");
      out.write("      </div>\n");
      out.write("    </div>\n");
      out.write("  </div>\n");
      out.write("                \n");
      out.write("                \n");
      out.write("        <div class=\"py-5\">\n");
      out.write("    <div class=\"container\">\n");
      out.write("      <div class=\"row\">\n");
      out.write("        <div class=\"col-md-12 d-flex justify-content-center\">\n");
      out.write("            <input type=\"submit\" class=\"btn btn-primary justify-content-center\" value=\"Sterge liniile selectate\" >\n");
      out.write("        </div>\n");
      out.write("      </div>\n");
      out.write("    </div>\n");
      out.write("  </div>\n");
      out.write("        </form>\n");
      out.write("        ");

            rs.close();
            jb.disconnect();
        
      out.write("\n");
      out.write("  <script src=\"https://code.jquery.com/jquery-3.2.1.slim.min.js\" integrity=\"sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN\" crossorigin=\"anonymous\"></script>\n");
      out.write("  <script src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js\" integrity=\"sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh\" crossorigin=\"anonymous\"></script>\n");
      out.write("  <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js\" integrity=\"sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1\" crossorigin=\"anonymous\"></script>\n");
      out.write("    </body>\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
