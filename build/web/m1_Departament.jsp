<%-- 
    Document   : m1_Departament
    Created on : Dec 23, 2018, 6:47:42 PM
    Author     : Asus
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page language="java" import="java.lang.*,java.math.*,db.*,java.sql.*, java.io.*, java.util.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <style>
body {
    background-image: url("silver.jpg");
    background-repeat: no-repeat;
    background-position: center;
    background-attachment: fixed;
    background-size: cover;
}
</style>
    <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css">
  <link rel="stylesheet" href="https://v40.pingendo.com/assets/bootstrap/bootstrap-4.0.0-beta.1.css" type="text/css"> </head>

    <jsp:useBean id="jb" scope="session" class="db.JavaBean" />
    <jsp:setProperty name="jb" property="*" />
    <body>
        <h1 align="center">Modifica Departament</h1>
        
        
        
        <div class="text-center text-capitalize">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <ul class="nav nav-pills align-items-center align-self-center text-center d-flex justify-content-center">
            <li class="nav-item text-center align-items-center justify-content-center align-self-center d-flex">
              <a href="index.html" class="nav-link active"> <i class="fa fa-home fa-home pull-left"></i>&nbsp;Pagina principala</a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
        <%
            jb.connect();
            String NumeProfesor, PrenumeProfesor, Mail, NumeCatedra, SefCatedra, NumeDepartament, SefDepartament;

            int aux = java.lang.Integer.parseInt(request.getParameter("primarykey"));
            ResultSet rs = jb.intoarceDepartamenteId(aux);
            rs.first();
            int id1 = rs.getInt("idprofesor_");
            int id2 = rs.getInt("idcatedra_");

            NumeProfesor = rs.getString("NumeProfesor");
            PrenumeProfesor = rs.getString("PrenumeProfesor");
            Mail = rs.getString("Mail");
            NumeCatedra = rs.getString("NumeCatedra");
            SefCatedra = rs.getString("SefCatedra");
            NumeDepartament = rs.getString("NumeDepartament");
            SefDepartament = rs.getString("SefDepartament");

            ResultSet rs1 = jb.vedeTabela("profesori");
            ResultSet rs2 = jb.vedeTabela("catedre");
            int idprofesor, idcatedra;


        %>
        <form action="m2_Departament.jsp" method="post">
         <div class="py-5">
    <div class="container">
      <div class="row d-flex justify-content-center">       
        <div class="col-md-6 offset-md-4">    
            <table align="center" class="table opaque-overlay"> 
                <form class="p-4 border border-primary bg-warning" method="post" action="https://formspree.io/">
                    
                    <th>
                        Selectati profesor
                        <SELECT NAME="idprofesor">
                            <%
                                while (rs1.next()) {
                                    idprofesor = rs1.getInt("idprofesor");
                                    NumeProfesor = rs1.getString("Nume");
                                    PrenumeProfesor = rs1.getString("Prenume");
                                    Mail = rs1.getString("Mail");
                                    if (idprofesor != id1) {
                            %>
                            <OPTION VALUE="<%= idprofesor%>"><%= idprofesor%>, <%= NumeProfesor%>, <%= PrenumeProfesor%>, <%= Mail%></OPTION>
                                <%
                                        } else {
                                %>                
                            <OPTION selected="yes" VALUE="<%= idprofesor%>"><%= idprofesor%>, <%= NumeProfesor%>, <%= PrenumeProfesor%>, <%= Mail%></OPTION>
                                <%
                                        }
                                    }
                                %>
                        </SELECT>
                    </th>
                    <th>
                    Selectati catedra
                        <SELECT NAME="idcatedra">
                            <%
                                while (rs2.next()) {
                                    idcatedra = rs2.getInt("idcatedra");
                                    NumeCatedra = rs2.getString("Nume");
                                    SefCatedra = rs2.getString("SefCatedra");
                            if (idcatedra != id2) {
                            %>
                            <OPTION VALUE="<%= idcatedra%>"><%= idcatedra%>, <%= NumeCatedra%>, <%= SefCatedra%></OPTION>
                                <%
                                        } else {
                                %>                
                            <OPTION selected="yes" VALUE="<%= idcatedra%>"><%= idcatedra%>, <%= NumeCatedra%>, <%= SefCatedra%></OPTION>
                                <%
                                        }
                                    }
                                %>
                        </SELECT>
                    </th>
                  
                
                    <div class="form-group" > 
                    <input type="text" class="form-control" name="iddepartament" value="<%= aux%>" readonly> </div>              

                    <div class="form-group" > 
                    <input type="text" class="form-control" name="NumeDepartament" value="<%= NumeDepartament%>" placeholder="Nume Departament"> </div>       
                    
                    <div class="form-group" > 
                    <input type="text" class="form-control" name="SefDepartament" value="<%= SefDepartament%>" placeholder="Sef Departament"> </div>              


                
                 </form>
            </table>
        </div>
      </div>
    </div>
  </div>
                
                
  <div class="py-5">
    <div class="container">
      <div class="row">
        <div class="col-md-12 d-flex justify-content-center">
            <input type="submit" class="btn btn-primary justify-content-center" value="Modifica Departament" >
        </div>
      </div>
    </div>
  </div>
            <div class="text-center text-capitalize">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <ul class="nav nav-pills align-items-center align-self-center text-center d-flex justify-content-center">
            <li class="nav-item text-center align-items-center justify-content-center align-self-center d-flex">
              <a href="index.html" class="nav-link active"> <i class="fa fa-home fa-home pull-left"></i>&nbsp;Pagina principala</a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
    </body>
    <%
        rs.close();
        rs1.close();
        rs2.close();
        jb.disconnect();
    %>
</html>l>