<%-- 
    Document   : sterge_Profesor
    Created on : Dec 23, 2018, 4:42:49 PM
    Author     : Asus
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page language="java" import="java.lang.*,java.math.*,db.*,java.sql.*, java.io.*, java.util.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <style> body {
    background-image: url("silver.jpg");
    background-repeat: no-repeat;
    background-position: center;
    background-attachment: fixed;
    background-size: cover;
}
</style>
    <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css">
  <link rel="stylesheet" href="https://v40.pingendo.com/assets/bootstrap/bootstrap-4.0.0-beta.1.css" type="text/css"> </head>
    <jsp:useBean id="jb" scope="session" class="db.JavaBean" />
    <jsp:setProperty name="jb" property="*" />
    <body>
        <%
            String[] s = request.getParameterValues("primarykey");
            jb.connect();
            jb.stergeDateTabela(s, "profesori", "idprofesor");
            jb.disconnect();
        %>
        
        
        
       <div class="py-5">
    <div class="container">
      <div class="row d-flex justify-content-center">
        <div class="col-md-4">
          <h2 class="text-center bg-light">Stergerea s-a efectuat cu succes</h2>
        </div>
      </div>
    </div>
  </div>

        <div class="text-center text-capitalize">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <ul class="nav nav-pills align-items-center align-self-center text-center d-flex justify-content-center">
            <li class="nav-item text-center align-items-center justify-content-center align-self-center d-flex">
              <a href="index.html" class="nav-link active"> <i class="fa fa-home fa-home pull-left"></i>&nbsp;Pagina principala</a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
        <br></br>
        <br></br>
        <div class="py-5">
    <div class="container">
      <div class="row">
        <div class="col-md-4 d-flex justify-content-end">
          <a href="tabela_Profesori.jsp" class="btn btn-primary">Tabela Profesori</a>
        </div>
        <div class="col-md-4 d-flex justify-content-center">
          <a href="tabela_Departamente.jsp" class="btn btn-primary">Tabela Departamente</a>
        </div>
        <div class="col-md-4">
          <a href="tabela_Catedre.jsp" class="btn btn-primary">Tabela Catedre</a>
        </div>
      </div>
    </div>
  </div>
    </body>
</html>