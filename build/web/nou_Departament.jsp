<%-- 
    Document   : nou_Departament
    Created on : Dec 23, 2018, 6:30:33 PM
    Author     : Asus
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page language="java" import="java.lang.*,java.math.*,db.*,java.sql.*, java.io.*, java.util.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <style>
body {
    background-image: url("silver.jpg");
    background-repeat: no-repeat;
    background-position: center;
    background-attachment: fixed;
    background-size: cover;
}
</style>
    <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css">
  <link rel="stylesheet" href="https://v40.pingendo.com/assets/bootstrap/bootstrap-4.0.0-beta.1.css" type="text/css"> </head>
    <jsp:useBean id="jb" scope="session" class="db.JavaBean" />
    <jsp:setProperty name="jb" property="*" />
   <body background="silver.jpg">
        <%
            int idprofesor, idcatedra;
            String id1, id2, NumeProfesor, PrenumeProfesor, Mail, NumeCatedra, SefCatedra, NumeDepartament, SefDepartament;
            id1 = request.getParameter("idprofesor");
            id2 = request.getParameter("idcatedra");
            NumeDepartament = request.getParameter("NumeDepartament");
            SefDepartament = request.getParameter("SefDepartament");
            if (id1 != null) {
                jb.connect();
                jb.adaugaDepartament(java.lang.Integer.parseInt(id1), java.lang.Integer.parseInt(id2), NumeDepartament, SefDepartament);
                jb.disconnect();
        %>
        <h1 align="center"> Departamentul a fost adaugata cu succes.</h1>
        <br></br>
        <br></br>
        <br></br>
        <div class="text-center text-capitalize">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <ul class="nav nav-pills align-items-center align-self-center text-center d-flex justify-content-center">
            <li class="nav-item text-center align-items-center justify-content-center align-self-center d-flex">
              <a href="index.html" class="nav-link active"> <i class="fa fa-home fa-home pull-left"></i>&nbsp;Pagina principala</a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div><%
      
      
        } else {
        jb.connect();
        ResultSet rs1 = jb.vedeTabela("profesori");
        ResultSet rs2 = jb.vedeTabela("catedre");
        %>
        <h1 align="center">Adaugare departament nou</h1>
        <form action="nou_Departament.jsp" method="post">
 <div class="py-5">
    <div class="container">
      <div class="row d-flex justify-content-center">       
        <div class="col-md-6 offset-md-4">    

            <table align="center" class="table opaque-overlay"> 
                <form class="p-4 border border-primary bg-warning" method="post" action="https://formspree.io/">
                    
                    <th>
                        
                        Selectati Profesorul:
			<SELECT NAME="idprofesor">
                                <%
                                    while(rs1.next()){
                                        idprofesor = rs1.getInt("idprofesor");
                                        NumeProfesor = rs1.getString("Nume");
                                        PrenumeProfesor = rs1.getString("Prenume");
                                        Mail = rs1.getString("Mail");
                                %>
                                    <OPTION VALUE="<%= idprofesor%>"><%= idprofesor%>,<%= NumeProfesor%>,<%= PrenumeProfesor%>,<%= Mail%></OPTION>
                                <%
                                    }
                                %>
                                    
                                    </th>
                                    
			</SELECT>
                    
                    
               
                <th>
                    
                     
                        Selectati catedra:
			<SELECT NAME="idcatedra">
				<!-- OPTION selected="yes" VALUE="iris1">Iris 1</OPTION -->
                                <%
                                    while(rs2.next()){
                                        idcatedra = rs2.getInt("idcatedra");
                                        NumeCatedra = rs2.getString("Nume");
                                        SefCatedra = rs2.getString("SefCatedra");
                                %>
                                    <OPTION VALUE="<%= idcatedra%>"><%= idcatedra%>,<%= NumeCatedra%>,<%= SefCatedra%></OPTION>
                                <%
                                    }
                                %>
			</SELECT>
                     
                </th>
                    
                       <div class="form-group" > 
                    <input type="text" class="form-control" name="NumeDepartament"  placeholder="NumeDepartament"> </div>       
                    
                       <div class="form-group" > 
                    <input type="text" class="form-control" name="SefDepartament"  placeholder="SefDepartament"> </div>              


                
                    
                
            </form>
            </table>
        </div>
      </div>
    </div>
  </div>
            
        <div class="py-5">
    <div class="container">
      <div class="row">
        <div class="col-md-12 d-flex justify-content-center">
            <input type="submit" class="btn btn-primary justify-content-center" value="Adauga Departament" >
        </div>
      </div>
    </div>
  </div>
  
                        
  <div class="text-center text-capitalize">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <ul class="nav nav-pills align-items-center align-self-center text-center d-flex justify-content-center">
            <li class="nav-item text-center align-items-center justify-content-center align-self-center d-flex">
              <a href="index.html" class="nav-link active"> <i class="fa fa-home fa-home pull-left"></i>&nbsp;Pagina principala</a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  </form>   
        <%
            }
        %>
        
    </body>
</html>