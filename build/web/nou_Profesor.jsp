<%-- 
    Document   : nou_Profesor
    Created on : Dec 23, 2018, 4:39:45 PM
    Author     : Asus
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page language="java" import="java.lang.*,java.math.*,db.*,java.sql.*, java.io.*, java.util.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head><style> body {
    background-image: url("silver.jpg");
    background-repeat: no-repeat;
    background-position: center;
    background-attachment: fixed;
    background-size: cover;
}
</style>
    <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css">
  <link rel="stylesheet" href="https://v40.pingendo.com/assets/bootstrap/bootstrap-4.0.0-beta.1.css" type="text/css"> 
    <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css">
  <link rel="stylesheet" href="https://v40.pingendo.com/assets/bootstrap/bootstrap-4.0.0-beta.1.css" type="text/css"></head>
    <jsp:useBean id="jb" scope="session" class="db.JavaBean" />
    <jsp:setProperty name="jb" property="*" />
    <body>
        
        <br/>
        <%
            String Nume = request.getParameter("Nume");
            String Prenume = request.getParameter("Prenume");
            String Mail = request.getParameter("Mail");
            if (Nume != null) {
                jb.connect();
                jb.adaugaProfesor(Nume, Prenume, Mail);
                jb.disconnect();
        %>
        
        <div class="py-5">
    <div class="container">
      <div class="row d-flex justify-content-center">
        <div class="col-md-6">
          <h2 class="text-center bg-light">Profesorul a fost adaugat cu succes</h2>
        </div>
      </div>
    </div>
  </div>
        <br></br>
        <br></br>
        <br></br>
        
   <div class="text-center text-capitalize">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <ul class="nav nav-pills align-items-center align-self-center text-center d-flex justify-content-center">
            <li class="nav-item text-center align-items-center justify-content-center align-self-center d-flex">
              <a href="index.html" class="nav-link active"> <i class="fa fa-home fa-home pull-left"></i>&nbsp;Pagina principala</a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
        
        <%
            
        } else {
        %>
        
        
        <div class="py-5">
    <div class="container">
      <div class="row d-flex justify-content-center">
        <div class="col-md-4">
          <h2 class="text-center bg-light">Adauga Profesor nou</h2>
        </div>
      </div>
    </div>
  </div>
         
         
        <div class="text-center text-capitalize">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <ul class="nav nav-pills align-items-center align-self-center text-center d-flex justify-content-center">
            <li class="nav-item text-center align-items-center justify-content-center align-self-center d-flex">
              <a href="index.html" class="nav-link active"> <i class="fa fa-home fa-home pull-left"></i>&nbsp;Pagina principala</a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
        <p> </p>
  <form action="nou_Profesor.jsp" method="post">      
        
      
      
      
   <div class="py-5">
    <div class="container">
      <div class="row d-flex justify-content-center">       
        <div class="col-md-6 offset-md-4">    
                <form class="p-4 border border-primary bg-warning" method="post" action="https://formspree.io/">
                   
            
            
            <div class="form-group"> 
              <input type="text" class="form-control" name="Nume" placeholder="Nume"> </div>
              
              
            <div class="form-group"> 
              <input type="text" class="form-control" name="Prenume"  placeholder="Prenume"> </div>
              
              
            <div class="form-group"> 
              <input type="text" class="form-control" name="Mail" placeholder="Mail"> </div>
              
              
            <button type="submit" class="btn mt-4 btn-block p-2 btn-primary"><b>Adauga profesorul</b></button> 
            
          
               </form>
            
        </div>
      </div>
    </div>
  </div>
        
        
              
        <%
            }
        %>
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
    </body>
</html>