<%-- 
    Document   : m1_Profesor
    Created on : Dec 23, 2018, 4:47:14 PM
    Author     : Asus
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page language="java" import="java.lang.*,java.math.*,db.*,java.sql.*, java.io.*, java.util.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <style> body {
    background-image: url("silver.jpg");
    background-repeat: no-repeat;
    background-position: center;
    background-attachment: fixed;
    background-size: cover;
}
</style>
    <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css">
  <link rel="stylesheet" href="https://v40.pingendo.com/assets/bootstrap/bootstrap-4.0.0-beta.1.css" type="text/css"> </head>

    <jsp:useBean id="jb" scope="session" class="db.JavaBean" />
    <jsp:setProperty name="jb" property="*" />
    <body>
        <div class="py-5">
    <div class="container">
      <div class="row d-flex justify-content-center">
        <div class="col-md-4">
          <h2 class="text-center bg-light">Modifica Profesor</h2>
        </div>
      </div>
    </div>
  </div>
        <br/>
  
        <div class="text-center text-capitalize">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <ul class="nav nav-pills align-items-center align-self-center text-center d-flex justify-content-center">
            <li class="nav-item text-center align-items-center justify-content-center align-self-center d-flex">
              <a href="index.html" class="nav-link active"> <i class="fa fa-home fa-home pull-left"></i>&nbsp;Pagina principala</a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
        <%
                jb.connect();
                int aux = java.lang.Integer.parseInt(request.getParameter("primarykey"));
                ResultSet rs = jb.intoarceLinieDupaId("profesori", "idprofesor", aux);
                rs.first();
                String Nume = rs.getString("Nume");
                String Prenume = rs.getString("Prenume");
                String Mail = rs.getString("Mail");
                rs.close();
                jb.disconnect();
           %>
        
     
<form action="m2_Profesor.jsp" method="post">
    
    
  <div class="py-5">
    <div class="container">
      <div class="row d-flex justify-content-center">       
        <div class="col-md-6 offset-md-4">    

            <table align="center">
               <form class="p-4 border border-primary bg-warning" method="post" action="https://formspree.io/">
               
                   
                 
                     <div class="form-group"> 
                         <input type="text" class="form-control" name="idprofesor"  value="<%= aux%>" readonly> </div>

                         <div class="form-group"> 
                        <input type="text" class="form-control" name="Nume" placeholder="Nume" value="<%= Nume%>"/> </div>
                           
                        <div class="form-group"> 
                        <input type="text" class="form-control" name="Prenume" placeholder="Prenume" value="<%= Prenume%>"/> </div>
                        
                        <div class="form-group"> 
                        <input type="text" class="form-control" name="Mail" placeholder="Mail" value="<%= Mail%>"/> </div>
               </form>
            </table>
        </div>
      </div>
    </div>
  </div>
  

               
   <div class="py-5">
    <div class="container">
      <div class="row">
        <div class="col-md-12 d-flex justify-content-center">
            <input type="submit" class="btn btn-primary justify-content-center" value="Salveaza modificari" >
        </div>
      </div>
    </div>
  </div>
        </form>
    </body>
</html>