<%-- 
    Document   : tabela_Profesori
    Created on : Dec 23, 2018, 4:35:02 PM
    Author     : Asus
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page language="java" import="java.lang.*,java.math.*,db.*,java.sql.*, java.io.*, java.util.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
     <style> body {
    background-image: url("silver.jpg");
    background-repeat: no-repeat;
    background-position: center;
    background-attachment: fixed;
    background-size: cover;
}
</style>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css">
  <link rel="stylesheet" href="https://v40.pingendo.com/assets/bootstrap/bootstrap-4.0.0-beta.1.css" type="text/css"> </head>


    <jsp:useBean id="jb" scope="session" class="db.JavaBean" />
    <jsp:setProperty name="jb" property="*" />
    <body>    
        <div class="py-5">
    <div class="container">
      <div class="row d-flex justify-content-center">
        <div class="col-md-4">
          <h2 class="text-center bg-light">Tabela Profesori</h2>
        </div>
      </div>
    </div>
  </div>

  <div class="text-center text-capitalize">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <ul class="nav nav-pills align-items-center align-self-center text-center d-flex justify-content-center">
            <li class="nav-item text-center align-items-center justify-content-center align-self-center d-flex">
              <a href="index.html" class="nav-link active"> <i class="fa fa-home fa-home pull-left"></i>&nbsp;Pagina principala</a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
             
             <div class="py-5">
    <div class="container">
      <div class="row">
        <div class="col-md-4 d-flex justify-content-end">
          <a href="tabela_Departamente.jsp" class="btn btn-primary">Tabela Departamente</a>
        </div>
        <div class="col-md-4 d-flex justify-content-center">
          <a href="nou_Profesor.jsp" class="btn btn-primary">Adauga un profesor nou</a>
        </div>
        <div class="col-md-4">
          <a href="tabela_Catedre.jsp" class="btn btn-primary">Tabela Catedre</a>
        </div>
      </div>
    </div>
  </div>
  
        
  <form action="sterge_Profesor.jsp" method="post">            
   <div class="py-5">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <table class="table opaque-overlay">
              <thead>
              <tr>
                <th class="bg-light">Mark:</th>
                <th class="bg-light">ID Profesort:</th>
                <th class="bg-light">Nume:</th>
                <th class="bg-light">Prenume:</th>
                <th class="bg-light">Mail:</th>
                </tr>
              </thead>
            
              
            <%
                    jb.connect();
                    ResultSet rs = jb.vedeTabela("profesori");
                    long x;
                    while (rs.next()) {
                        x = rs.getInt("idprofesor");
                %>
                <tbody>
              <tr>
                    <td class="bg-light"><input type="checkbox" name="primarykey" value="<%= x%>" /></td><td class="bg-light"><%= x%></td>
                    <td class="bg-light"><%= rs.getString("Nume")%></td>
                    <td class="bg-light"><%= rs.getString("Prenume")%></td>
                    <td class="bg-light"><%= rs.getString("Mail")%></td>
                      
                      <%
                        }
                    %>
                    </tr>
                </tbody>  

          </table>
        </div>
      </div>
    </div>
  </div>
                
                
        <div class="py-5">
    <div class="container">
      <div class="row">
        <div class="col-md-12 d-flex justify-content-center">
            <input type="submit" class="btn btn-primary justify-content-center" value="Sterge liniile selectate" >
        </div>
      </div>
    </div>
  </div>
        </form>
        <%
            rs.close();
            jb.disconnect();
        %>
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
    </body>
</html>